#include <stdio.h>
#include <stdint.h>

#define FORCE_UINT8_T __attribute__ ((__packed__))
#define __IO volatile

uint8_t go = 0;
uint8_t stop = 0;
uint8_t timeout = 0;

uint8_t red = 0;
uint8_t green = 0;
uint8_t yellow = 0;

typedef enum FORCE_UINT8_T {
    NO_EVENT,
    GO,
    STOP,
    TIMEOUT
} Event;

Event event = NO_EVENT;

typedef enum FORCE_UINT8_T {
    RED,
    GREEN,
    YELLOW,
    NUMBER_OF_STATES
} State;

State current = GREEN;

typedef union {
    struct {
        State current;
        State go;
        State stop;
        State timeout;
        __IO uint8_t red;
        __IO uint8_t green;
        __IO uint8_t yellow;
    } led;
    State nextState[NUMBER_OF_STATES];
} StateEntry;

void checkEvents() {
    if(go) {
        event = GO;
    } else if(stop) {
        event = STOP;
    } else if (timeout) {
        event = TIMEOUT;
    } else {
        event = NO_EVENT;
    }
}

int main(int argc, char *argv[])
{
    const StateEntry states[NUMBER_OF_STATES] = {
        {RED, GREEN, RED, RED, 1, 0, 0},
        {GREEN, GREEN, YELLOW, GREEN, 0, 1, 0},
        {YELLOW, YELLOW, YELLOW, RED, 0, 0, 1}
    };

    while(1) {
        checkEvents();
        red = states[current].led.red;
        green = states[current].led.green;
        yellow = states[current].led.yellow;
        current = states[current].nextState[event];
    }

    return 0;
}
