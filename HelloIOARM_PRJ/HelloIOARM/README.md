Run the following command to start the debugging tool:
    $ openocd -f /usr/local/Cellar/open-ocd/0.10.0/share/openocd/scripts/interface/stlink-v2.cfg -f /usr/local/Cellar/open-ocd/0.10.0/share/openocd/scripts/target/stm32f1x.cfg

References:
1. https://www.bartslinger.com/cx-10-quadcopter/debugging-stm32-from-qtcreator/
