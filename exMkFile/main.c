/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  This is an example
 *
 *        Version:  1.0
 *        Created:  23/02/2017 10:42:31
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Rodrigo Peixoto 
 *   Organization:  UFAL
 *
 * =====================================================================================
 */
#include <stdio.h>
#include "version.h"
int main() {
#ifdef TEST
	int a = 10;
#else
	int a = 5;
#endif
	printf("Version: %d.%d.%d\n", MAJOR,  MINOR,  BUILD );
	return 0;
}

