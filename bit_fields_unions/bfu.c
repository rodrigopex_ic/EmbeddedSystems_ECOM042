#include <stdio.h>

#define RED_STATE_DATA     0b00000001
#define GREEN_STATE_DATA   0b00001010
#define YELLOW_STATE_DATA  0b00010100
#define UNKNOWN_STATE_DATA 0b00011111

typedef enum {
    RED,
    GREEN,
    YELLOW,
    UNKNOWN
} State;

typedef union _stoplight {
    struct _stoplight_internal{
        volatile unsigned char red: 1;
        volatile unsigned char green: 1;
        volatile unsigned char yellow: 1;
        volatile State state: 2;
    } status;
    volatile unsigned char data;
} Stoplight;

Stoplight stoplight;

volatile unsigned char stop = 0, go = 0, timeout = 0; //INPUTS EVENTS

void nextState() {
    switch(stoplight.status.state) {
        case RED:
            if(go) stoplight.status.state = GREEN;
        break;
        case GREEN:
            if(stop) stoplight.status.state = YELLOW;
        break;
        case YELLOW:
            if(timeout) stoplight.status.state = RED;
        break;
        default:
            stoplight.status.state = GREEN;
    }
}

void fsm() {
    switch(stoplight.status.state) {
        case RED:
            stoplight.data = RED_STATE_DATA;
        break;
        case GREEN:
            stoplight.data = GREEN_STATE_DATA;
        break;
        case YELLOW:
            stoplight.data = YELLOW_STATE_DATA;
        break;
        default:
            stoplight.data = UNKNOWN_STATE_DATA;
    }
}

int main(void) {
    stoplight.status.state = GREEN;
    printf("size of stoplight %ld", sizeof(stoplight));
//    while(1) {
//        fsm();
//        nextState();
//    }
	return 0;
}

