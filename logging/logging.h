#ifndef LOGGING_H
#define LOGGING_H

#include<stdint.h>

enum eLogSubSystem {CONNECTION,  STORAGE};
enum eLogLevel {DEBUG, INFO, WARNNING, CRITIICAL};

#define MAJOR 1
#define MINOR 0
#define BUILD 180

typedef struct {
    uint8_t major;
    uint8_t minor;
    uint16_t build;
} FirmwareVersion;

void log(enum eLogSubSystem sys, enum eLogLevel level, const char *msg);
void logWithNum(enum eLogSubSystem sys, enum eLogLevel level, const char *msg, int num);
void logSetOutputLevel( enum eLogSubSystem sys, enum eLogLevel level);
void logGlobalOn();
void logGlobalOff();
void logFirmwareVersion();
FirmwareVersion *firmwareVersion();
void FirmwareVersion_create(int major, int minor, int build);

#endif // LOGGING_H
